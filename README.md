### Batalha Naval 2.0


A IDE que foi utilizada para o desenvolvimento do projeto foi o "Netbeans IDE 8.1".

1. Abra o programa no Netbeans.

2. Vá na pasta view e selecione o botão Run no arquivo Main.java.

  ------ Como Jogar ------

3. Escolha uma das duas opções do menu principal:

     ------ Menu Principal ------
    * Mapa:
    Nesta opção você devera escolher um arquivo dentro das especificações do projeto (Esta limitado a projetos com matrizes com o numero de linhas igual a o de colunas).

    * Aleatório:
    Nesta opção será gerado aleatoriamente um mapa dentre os 5 contidos no sistema.

4. Agora e só jogar:

### Observações:

  1. O menu principal funciona corretamente, enviando todos os dados necessários para se consolidar o jogo.

  2. O jogo não esta funcionando perfeitamente ao clicar nos botões não ocorrera a diversão que é jogar.
