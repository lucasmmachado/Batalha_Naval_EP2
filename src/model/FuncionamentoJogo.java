/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author lucas
 */
public class FuncionamentoJogo extends Campo{
    private int [][] tiros;
    private int recursos;
    private int turno;
    
    public int getRecursos() {
        return recursos;
    }

    public void setRecursos(int recursos) {
        this.recursos = recursos;
    }

    public int getTurno() {
        return turno;
    }

    public void setTurno(int turno) {
        this.turno = turno;
    }
    
    public void inicializaTiros(int linhas, int colunas) {
        tiros = new int[linhas][colunas];
    }
    
    public int[][] getTiros() {
        return tiros;
    }

    public void setTiros(int[][] tiros) {
        this.tiros = tiros;
    }
    
    public int getTiros(int linha, int coluna) {
        return tiros[linha][coluna];
    }
    
    public void setTiros(int linha, int coluna, int valor) {
        tiros[linha][coluna] = valor;
    }
    
}
