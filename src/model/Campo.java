/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author lucas
 */
public class Campo {
    private int numLinhas;
    private int numColunas;
    private int [][] matriz;
    private int [] navios = new int [6];


    public void inicializaMatriz(int linhas, int colunas) {
        matriz = new int [linhas][colunas];
    }
    
    public int[][] getMatriz() {
        return matriz;
    }

    public void setMatriz(int[][] matriz) {
        this.matriz = matriz;
    }
    
    public int getMatriz(int linha, int coluna) {
        return matriz[linha][coluna];
    }

    public void setMatriz(int linha, int coluna, int elemento) {
        matriz[linha][coluna] = elemento;
    }

    public int getNavios(int quantNavio) {
        return navios[quantNavio];
    }
    
    public void setNavios(int tipoNavio, int quantNavio) {
        navios[tipoNavio] = quantNavio;
    }
    
    public int[] getNavios() {
        return navios;
    }

    public void setNavios(int[] navios) {
        this.navios = navios;
    }

    public File getArquivo() {
        return arquivo;
    }

    public void setArquivo(File arquivo) {
        this.arquivo = arquivo;
    }
    private File arquivo = null;
    
    
    public int getNumLinhas() {
        return numLinhas;
    }

    public void setNumLinhas(int valor) {
        this.numLinhas = valor;
    }

    public int getNumColunas() {
        return numColunas;
    }

    public void setNumColunas(int valor) {
        this.numColunas = valor;
    }
   
    
    public String ler(File arquivo){
    
        String conteudo = "";

        try {
            FileReader arquivo1 = new FileReader(arquivo);
            BufferedReader lerArquivo = new BufferedReader(arquivo1);
            String linha="";
            
            try{
                linha = lerArquivo.readLine();
                linha = lerArquivo.readLine();
                String a = linha.substring(0,linha.indexOf(' '));
                String b = linha.substring(linha.indexOf(' ')+1);
                int temp1 = Integer.parseInt(a);
                int temp2 = Integer.parseInt(b);
                setNumLinhas(temp1);
                setNumColunas(temp2);
                
                matriz = new int[getNumLinhas()][getNumColunas()];
                
                while(linha!=null){
                    linha = lerArquivo.readLine();
                    conteudo += linha;
                }
                
                int CONST = 8;
                
                for(int i = 0; i < getNumColunas(); i++){
                    for(int j = 0; j < getNumLinhas(); j++){
                        char TEMP = conteudo.charAt(CONST);
                        
                        setMatriz(i, j, Character.getNumericValue(TEMP));
                        CONST++;
                    }
                }
                                
                CONST = 8 + getNumColunas()*getNumLinhas() + 32;
                
                for(int i = 1 ; i < 6 ;i++ ){
                    char TEMP = conteudo.charAt(CONST);                    
                    setNavios(i,Character.getNumericValue(TEMP));
                    CONST += 3;
                }
                
                System.out.println(conteudo);
                arquivo1.close();                                   
                return conteudo;
                } catch (IOException ex) {
                    System.out.println("Erro: Não foi possível ler o arquivo.");
                    return "";
                }                
            } catch (FileNotFoundException ex) {
                System.out.println("Erro: O arquivo não encontrado.");
                return "";
            }
        }
    
    
}
