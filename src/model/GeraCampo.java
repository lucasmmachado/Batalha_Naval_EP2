/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Random;

/**
 *
 * @author lucas
 */
public class GeraCampo extends Campo {
    
    public void campoAleatorio(){

        int numMapa;

        Random random = new Random();

        int mapa = random.nextInt(4);
        
        
        
        switch (mapa){
            case 0:
                inicializaMatriz ( 9 , 9);

                setNumLinhas(9);
                setNumColunas(9);

                int [][]matriz1 = {{0,0,0,0,0,0,0,0,0},{0,1,0,0,0,2,2,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,4,0,0,0,0},{0,0,0,0,4,0,0,0,0},{0,0,0,0,4,0,0,0,0},{0,2,0,0,4,0,0,0,0},{0,2,0,0,0,0,0,1,0}};
                int [] navios1 = {0,2,2,0,1,0};
                setMatriz(matriz1);
                setNavios(navios1);

                break;
            case 1:
                inicializaMatriz ( 10 , 10);

                setNumLinhas(10);
                setNumColunas(10);

                int [][] matriz2 = {{0,0,0,0,0,0,0,0,0,0},{0,0,0,0,1,0,0,0,0,0},{0,1,0,2,2,0,0,0,0,1},{0,0,0,1,0,0,0,0,0,0},{0,0,3,0,0,0,0,0,0,0},
                                   {0,0,3,0,4,0,0,0,0,0},{0,0,3,0,4,0,0,0,0,0},{0,0,0,0,0,4,0,0,0,0},{0,0,2,0,0,4,0,0,0,0},{0,0,2,0,0,0,0,0,1,0}};
                int [] navios2 = {0,4,2,1,1,0};
                setMatriz(matriz2);
                setNavios(navios2);

                break;
            case 2:
                inicializaMatriz ( 8 , 8);

                setNumLinhas(8);
                setNumColunas(8);

                int [][] matriz3 = {{0,0,5,5,5,5,5,0},{2,2,0,1,0,0,0,0},{0,0,3,0,0,0,2,0},{0,0,3,0,4,0,2,0},
                                    {0,0,3,0,4,0,0,0},{0,0,0,0,0,4,0,3},{0,0,2,0,0,4,0,3},{0,0,2,0,0,0,1,3}};
                int [] navios3 = {0,2,3,2,1,1};
                setMatriz(matriz3);
                setNavios(navios3);

                break;
            case 3:
                inicializaMatriz ( 15 , 15);

                setNumLinhas(15);
                setNumColunas(15);

                int [][] matriz4 = {{0,0,5,5,5,5,5,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0,0,0,0,1,0,0},{0,0,2,0,0,0,1,0,0,0,3,3,3,0,0},{0,0,2,0,0,0,0,0,0,0,0,0,0,0,0},{0,0,0,4,4,4,4,0,0,0,0,0,2,2,0},
                                    {0,0,0,0,0,0,0,0,2,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,2,0,0,0,0,0,0},{0,0,0,0,0,3,3,3,0,0,0,0,0,0,0},{0,2,0,0,0,0,0,0,0,0,0,0,1,0,0},{0,2,0,0,0,0,0,0,0,0,0,0,0,0,0},
                                    {0,0,0,0,0,0,0,0,0,0,0,0,0,0,5},{0,0,0,1,0,0,0,0,0,0,0,0,0,0,5},{0,0,0,0,0,3,0,0,0,0,0,0,0,0,5},{0,0,0,0,0,3,0,0,0,0,0,0,0,0,5},{0,0,0,0,0,3,0,0,0,0,0,0,0,0,5}};
                int [] navios4 = {0,4,4,2,1,2};
                setMatriz(matriz4);
                setNavios(navios4);
                
            default:
                inicializaMatriz ( 7 , 7);

                setNumLinhas(7);
                setNumColunas(7);

                int [][] matriz5 = {{0,0,0,0,2,2,0},{0,0,0,3,0,2,0},{0,0,0,3,0,2,0},{0,0,0,3,0,0,0},
                                    {0,0,0,0,0,0,3},{0,0,2,0,0,0,3},{0,0,2,0,0,1,3}};
                int [] navios5 = {0,1,3,2,0,0};
                setMatriz(matriz5);
                setNavios(navios5);

                break;
        }
    }


}
