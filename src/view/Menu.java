/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Acoes;
import java.awt.Font;
import java.awt.Insets;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.filechooser.FileSystemView;

/**
 *
 * @author lucas
 */
public class Menu {
    
    private JFrame mainFrame;
    private JFileChooser mainFile;
    private JButton button1;
    private JButton button2;
    private JLabel label1; 
    private JLabel label2; 
    private Acoes Listener; 
   
    public final int JANELA_LARGURA = 300;
    public final int JANELA_ALTURA = 250;
    
    public Menu(){
        Listener = new Acoes();
        
        mainFrame = new JFrame("Batalha Naval");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setSize(JANELA_LARGURA,JANELA_ALTURA);
        mainFrame.setLayout(null);
        mainFrame.setLocationRelativeTo(null);
        
        mainFile = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
        Listener.saidas(mainFrame, mainFile);
        
        button1 = new JButton("Mapa");
        button1.setBounds(90,75,100,50);
        button1.setActionCommand("Abrir");
        button1.addActionListener(Listener);
        mainFrame.add(button1, "center");
        
        button2 = new JButton("Aletorio");
        button2.setBounds(90,145,100,50);
        button2.setActionCommand("Abrir Aleatorio");
        button2.addActionListener(Listener);
        mainFrame.add(button2, "center");

        label1 = new javax.swing.JLabel();
        label1.setFont(new java.awt.Font("MV Boli", 1, 24)); // NOI18N
        label1.setText("Batalha");
        label1.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
        label1.setBounds(100,2,100,60);
        
        mainFrame.add(label1,null );
        
        label2 = new javax.swing.JLabel();
        label2.setFont(new java.awt.Font("MV Boli", 1, 24)); // NOI18N
        label2.setText("Naval");
        label2.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
        label2.setBounds(107,22,100,60);
        
        mainFrame.add(label2,null );
        
        
        mainFrame.setVisible(true);
                   
    }
   
    
}
