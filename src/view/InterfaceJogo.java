/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.Acoes;
import controller.AcoesJogador;
import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.filechooser.FileSystemView;
import javax.xml.bind.Marshaller.Listener;
import model.FuncionamentoJogo;
import model.GeraCampo;
/**
 *
 * @author lucas
 */
public class InterfaceJogo {
    private JFrame mainFrame;
    private JPanel mainPainel;
    private JLabel label1;
    private JLabel label2;
    private JLabel label3;
    private JLabel label4;
    private JLabel label5;
    private JLabel label6;
    private JLabel label7;
    
    private AcoesJogador Listener; 
    
    public final int JANELA_LARGURA = 750;
    public final int JANELA_ALTURA = 430;


    public InterfaceJogo(int numLinhas , int numColunas){
        //int numLinhas = campo1.getNumLinhas();
        //int numColunas = campo1.getNumColunas();
        
        Listener = new AcoesJogador();
        
        mainFrame = new JFrame("Batalha Naval");
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setSize(JANELA_LARGURA,JANELA_ALTURA);
        mainFrame.setLayout(null);
        mainFrame.setLocationRelativeTo(null);
        
        mainPainel = new JPanel();
        mainPainel.setLayout(new GridLayout(numLinhas,numColunas));
        mainPainel.setBounds(300,50,400,300);
        mainPainel.setBorder(javax.swing.BorderFactory.createTitledBorder("Mapa"));

        JButton [][] posicao = new JButton [numLinhas][numColunas];
        
        for (int i = 0; i < numLinhas;i++){
            for (int j = 0; j < numColunas; j++){
                String linha = "" + i;
                String coluna = "" + j;
                
                String saida = "" + linha + " " + coluna;
                /*
                if (funcionamento1.getTiros(i, j) != 1){
                    posicao[i][j] = new JButton();
                    posicao[i][j].setBackground(Color.red);
                    posicao[i][j].setActionCommand(saida);
                    posicao[i][j].addActionListener(Listener);
                    mainPainel.add(posicao[i][j]);
                }else{
                    */
                    posicao[i][j] = new JButton();
                    posicao[i][j].setBackground(Color.blue);
                    posicao[i][j].setActionCommand(saida);
                    posicao[i][j].addActionListener(Listener);
                    mainPainel.add(posicao[i][j]);
                //}
            }
        }
        /*
        mainPainel = new JPanel();
        mainPainel.setLayout(new GridLayout(numLinhas, 1));
        mainPainel.setBounds(10,50,20,300);
        mainPainel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        
        JButton [] linhas = new JButton [numLinhas];

        for (int i = 0; i < numLinhas;i++){
            String linha = "" + i;
            String saida = "" + linha;

            linhas[i] = new JButton();
            linhas[i].setBackground(Color.blue);
            linhas[i].setActionCommand(saida);
            linhas[i].setText(linha);

            //posicao[i].addActionListener(Listener);
            mainPainel.add(linhas[i]);
        }        
        */
        label1 = new javax.swing.JLabel();
        label1.setFont(new java.awt.Font("MV Boli", 1, 24)); // NOI18N
        label1.setText("Batalha");
        label1.setFont(new Font("Trebuchet MS", Font.BOLD, 18));
        label1.setBounds(100,2,100,60);
        
        mainFrame.add(label1,null );
        
        label2 = new javax.swing.JLabel();
        label2.setFont(new java.awt.Font("MV Boli", 1, 24)); // NOI18N
        label2.setText("Naval");
        label2.setFont(new Font("Trebuchet MS", Font.BOLD, 18));
        label2.setBounds(107,22,100,60);
        
        mainFrame.add(label2, null );
        
        label3 = new javax.swing.JLabel();
        label3.setFont(new java.awt.Font("MV Boli", 1, 24)); // NOI18N
        label3.setText("Mapa:");
        label3.setFont(new Font("Trebuchet MS", Font.BOLD, 18));
        label3.setBounds(100,70,100,60);
        
        mainFrame.add(label3, null );
        
        String palavra = "" + numLinhas + " x " + numColunas;
        label4 = new javax.swing.JLabel();
        label4.setFont(new java.awt.Font("MV Boli", 1, 24)); // NOI18N
        label4.setText(palavra);
        label4.setFont(new Font("Trebuchet MS", Font.BOLD, 18));
        label4.setBounds(103,95,100,60);
        
        mainFrame.add(label4, null );        
       int recursos = 20;
        String palavra2 = "" + numLinhas + " x " + numColunas;
        label5 = new javax.swing.JLabel();
        label5.setFont(new java.awt.Font("MV Boli", 1, 24)); // NOI18N
        label5.setText("Recursos: " + recursos);
        label5.setFont(new Font("Trebuchet MS", Font.BOLD, 13));
        label5.setBounds(100,125,100,60);
        
        mainFrame.add(label5, null );        
        
        int acertos = 0;
        
        label6 = new javax.swing.JLabel();
        label6.setFont(new java.awt.Font("MV Boli", 1, 24)); // NOI18N
        label6.setText("Acertos: " + acertos);
        label6.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
        label6.setBounds(100,160,135,60);
        
        mainFrame.add(label6, null );        

        int rodada = 1;
        
        label7 = new javax.swing.JLabel();
        label7.setFont(new java.awt.Font("MV Boli", 1, 24)); // NOI18N
        label7.setText("Rodada: " + rodada);
        label7.setFont(new Font("Trebuchet MS", Font.BOLD, 14));
        label7.setBounds(100,195,180,60);
        rodada++;
        mainFrame.add(label7, null );        

        
        mainFrame.add(mainPainel);
        
        mainFrame.setVisible(true);
        
    }

}
