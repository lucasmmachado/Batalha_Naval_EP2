/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import model.FuncionamentoJogo;
import model.GeraCampo;
import view.InterfaceJogo;
/**
 *
 * @author lucas
 */
public class Acoes implements ActionListener {
    
    private JFileChooser mainFile;
    private JFrame mainFrame;
    private File arquivo;
    GeraCampo campoFuncoes = new GeraCampo();
    
    public void saidas(JFrame mainFrame, JFileChooser mainFile){
        this.mainFrame = mainFrame;
        this.mainFile = mainFile;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();
        FuncionamentoJogo funcionamento1 = new FuncionamentoJogo();
        
        if(comando.equals("Abrir")){
            int returnaValor = mainFile.showOpenDialog(mainFile);
            
            if(returnaValor == JFileChooser.APPROVE_OPTION){
                arquivo = mainFile.getSelectedFile();
                campoFuncoes.ler(arquivo);                
            }
        }else if(comando.equals("Abrir Aleatorio")){
            campoFuncoes.campoAleatorio();
        }

        InterfaceJogo jogo = new InterfaceJogo(campoFuncoes.getNumLinhas(), campoFuncoes.getNumColunas());
        
    }
}
